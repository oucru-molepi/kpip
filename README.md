# Potential immunogenic proteins of *Klebsiella pneumoniae*

Tools:
- CD-HIT v4.8.1 (built on Aug  7 2022) (conda env tormes-1.3.0)

Targeted proteins:
| #  | Gene name | Protein name                                                |
|----|-----------|-------------------------------------------------------------|
| 1  | *fepA*      | Siderophore enterobactin receptor                           |
| 2  | *fepB*      | Fe2+-enterobactin ABC transporter substrate-binding protein |
| 3  | *yidR*      | Adhesin                                                     |
| 4  |             | Colicin I receptor                                          |
| 5  | *fecA*      | TonB-dependent Fe(3+) dicitrate receptor                    |
| 6  | *fimH*      | Fimbrial chaperone protein (type I fimbriae)                |
| 7  | *mrkA*      | Type III fimbriae major subunit                             |
| 8  | *ompA*      | Porin                                                       |
| 9  | *ompN*      | Outer membrane protein N                                    |
| 10 | *phoE*      | Phosphoporin                                                |
| 11 | *cusC*      | Cation efflux system protein                                |

