| Target | # clusters | Cluster sizes and sequence lengths                                         | Similarity between representative sequences | Similarity between conserved Kp and *E. coli* sequences                                                                                                                                                                                        |
| ------ | ---------- | -------------------------------------------------------------------------- | ------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| *fimH* | 1          | **Cluster 0 (100%)**: 302aa (n=1725), 301aa (n=337)                        | -                                           | Kp: CAI3912655.1 (302aa)<br/>E. coli: only 4 sequences with 99% coverage & >97% identity (UMX58120.1, MBX8949145.1, UYM69228.1, UMX54193.1); others have <90% identity                                                                         |
| *fecA* | 2          | Cluster 0 (39.1%): 774aa (n=836)<br/>**Cluster 1 (60.9%)**: 701aa (m=1303) | 76% coverage, 23.48% identity               | WP_285279650.1 (774aa, Cluster 0) matched 100 E. coli sequences with 100% coverage, >98% identity.<br/>CAI3915550.1 (701aa, Cluster 1) matched only 3 E. coli sequences with 100% coverage, >99% identity (UYM65963.1, UMX58353.1, UMX54371.1) |

*CD-HIT clusters: 95% identity cut-off

*Similarity was assessed by BLASTP

*E. coli (taxid:562). Database: Non-redundant protein sequences (nr)

*conserved Kp sequence: sequence from the largest cluster
