#!/bin/bash
summary_from_fasta(){
# Output a summary table (tab-delimited) for 
# all sequences in a fasta sequence file
# Input #1: name of fasta sequence file
    input_file=$1;

    # heading
    echo -e accession_number"\t"species"\t"length;

    awk -v RS=">" -F"[][]" -v OFS="\t" 'NR>1 {
        split($1,a," ");
        gsub(/\n/,"",$3); 
        print a[1], $2, length($3)}' $input_file;
}

kp_summary(){
# Output summary table (tab-delimited) for 
# sequences of only Klebs pneumo
# Input #1: name of fasta sequence file
    input_file=$1;

    # heading
    echo -e accession_number"\t"species"\t"length;

    # awk -v RS=">" -F"[][]" -v OFS="\t" 'NR>1 {
    #     split($1,a," ");
    #     split($2,sp," ");
    #     gsub(/\n/,"",$3); 
    #     if (sp[1]=="Klebsiella" && sp[2]=="pneumoniae") 
    #         {print a[1], $2, length($3)}}' $input_file;
    summary_from_fasta $input_file | tail -n +2 | \
        awk '{FS="\t"; OFS="\t"} {
            split($2,sp," ");
            if (sp[1]=="Klebsiella" && sp[2]=="pneumoniae") 
                {print $1,$2,$3}
        }';
}

len_dist_from_summary(){
# Return a sorted counts for protein seq lengths
# from a summary table (with a headling line), 
# provided through a stdin (piped) or a file (input #1)
    # Check to see if a pipe exists on stdin.
    if [ -p /dev/stdin ]; then
        tail -n +2 - > /tmp/len_dist_from_summary.txt;
        input_file="/tmp/len_dist_from_summary.txt";    
    else
    # Checking to ensure a filename was specified and that it exists
        if [ -f "$1" ]; then
            input_file="$1";
        else
            echo "No input given! Please provide a summary table."
        fi;
    fi;

    n_seq=$(cat $input_file | wc -l);
    echo -e length"\t"count"\t"percentage;
    cut -f3 $input_file | sort | uniq -c | sort -r |\
        awk -v n=$n_seq '{OFS="\t"}{
            print $2,$1,$1/n*100
        }';
    
    if [ "$input_file" = "/tmp/len_dist_from_summary.txt" ]; then
        rm $input_file;
    fi;
}

seq_from_IDlist(){
# Retrieve FASTA sequences from a list file of sequence IDs
# Can only work on single-line sequences
# Multi-line FASTA can be converted with `ml2sl_fasta`
    fasta_file="$1";
    list_file="$2";
    # awk -v lf="$list_file" -v RS=">" 'BEGIN {
    #     while((getline k < lf) >0) i[k]=1
    #     } {if (i[$1]) print ">"$0}' $fasta_file;
    awk -v lf="$list_file" 'BEGIN {
        while((getline k < lf) >0) i[k]=1
        } {split($1,n,">"); 
        if (i[n[2]]) check=0; 
        if (check < 2) {print $0; check+=1}}' $fasta_file;
}

extract_majorKp_seq(){
# Extract major sequences of K. pneumoniae (Kp)
# Input #1: name of fasta sequence file
# Input #2: an array of sequence lengths to extract
    input_file="$1";
    shift;
    len_arr=("$@");

    # Standalone method
    awk -v la="${len_arr[*]}" -v RS=">" -F"[][]" 'BEGIN {
        split(la, lenVal, " ");
        for (i in lenVal) lenKey[lenVal[i]]="";
        }
        NR>1 {
        split($2,sp," ");
        gsub(/\n/,"",$3); 
        if ((sp[1]=="Klebsiella" && sp[2]=="pneumoniae") && 
            length($3) in lenKey) 
            {print ">"$1,"["$2"]","\n"$3}}' $input_file;
}

extract_defined_nonKp_seq(){
# Extract sequences of Klebsiella species different from K. pneumoniae (non-Kp)
# with clearly defined species
# Input #1: name of fasta sequence file
    input_file="$1";

    awk -v RS=">" -F"[][]" 'NR>1 {
        split($2,sp," ");
        gsub(/\n/,"",$3);
        if (length(sp)>=2 && sp[1]=="Klebsiella" && 
            sp[2]!="pneumoniae" && sp[2]!="sp."){
                print ">"$1,"["$2"]","\n"$3
            }}' $input_file;
}

summary_clustering(){
# Create a table (tab-delimited) summarizing
# results of CD-HIT clustering
# Input #1: .clstr file
# Input #2: summary table file for the FASTA sequences 
#           (generated from `summary_from_fasta` or `kp_summary` func)
# Input #3: output file name

    clstr_file="$1";
    summary_file="$2";
    output_file="$3";

    echo -e cdhit_cluster"\t"accession_number"\t"species"\t"length"\t"identity > \
        $output_file;
    
    while read -r line; do
        if [[ "$line" =~ \>Cluster* ]]; then
            c=$(echo "$line" | sed 's/>//g');
            # echo "$c";
        else 
            acc=$(echo "$line" | cut -f2 | cut -d " " -f2 | 
                    sed 's/>//;s/\.\.\.//');
            len=$(echo "$line" | cut -f2 | cut -d " " -f1 | 
                    sed 's/aa,//');
            id=$(echo "$line" | cut -f2 | cut -d " " -f4 |
                    sed 's/%//');
            if [ "$id" == "" ]; then id="NA"; fi;
            sp=$(grep -w "$acc" $summary_file | cut -f2 | cut -d " " -f1,2);
            echo -e $c"\t"$acc"\t"$sp"\t"$len"\t"$id >> \
                $output_file;
        fi;
    done < $clstr_file;
}