#!/bin/bash
quarto render report.qmd

if [ $? -eq 0 ]; then
    echo "Rendering succeeded.";
else
    echo "Rendering failed.";
    exit 1;
fi

# Move rendered HTML files to public/
rm -r public/*
mv report.html public/
mv report_files/ public/report_files/

mkdir -p public/jupyter_book/
mv jupyter_book/*.html public/jupyter_book/

if [ $? -eq 0 ]; then
    echo "Structuring rendered files succeeded.";
else
    echo "Structuring rendered files failed.";
    exit 1;
fi

# Commit and push
git add public/
git commit -m "Quarto published"
git push origin main