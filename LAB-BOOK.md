# Lab Book

## 02-06 June, 2023: analysis procedure setup, on *fepB* 
*fepB*: Fe2+-enterobactin ABC transporter substrate-binding protein
Working directory: `test_fepB`

Search query on NCBI: `(Klebsiella[Organism]) AND fepB[Gene Name]`  
Download date: 02 Jun 2023

Get a tab-delimited summary file for all the sequences - accession number, species name, and the number of amino acids (protein sequence length) 
```bash
echo -e accession_number"\t"species"\t"length > fepB.Klebs.summary.tsv
awk -v RS=">" -F"[][]" -v OFS="\t" 'NR>1 {
    split($1,a," ");
    gsub(/\n/,"",$3); 
    print a[1], $2, length($3)}' fepB.Klebs.sequences.fasta >>\
    fepB.Klebs.summary.tsv
```

### *Klebsiella pneumoniae* only

Extract information of only *Klebsiella pneumoniae* sequences. Get a look at the sequence length distribution.
```bash
echo -e accession_number"\t"species"\t"length > fepB.Klebs_pneumo.summary.tsv
awk -v RS=">" -F"[][]" -v OFS="\t" 'NR>1 {
    split($1,a," ");
    split($2,sp," ");
    gsub(/\n/,"",$3); 
    if (sp[1]=="Klebsiella" && sp[2]=="pneumoniae") 
    {print a[1], $2, length($3)}}' fepB.Klebs.sequences.fasta >>\
    fepB.Klebs_pneumo.summary.tsv

tail -n +2 fepB.Klebs_pneumo.summary.tsv | cut -f3 | sort -n | uniq -c
```
The majority (5141/5246) of Klebs pneumo sequences has length of 319 aa, and other 58 sequences have 305aa. Other sequence lengths different from 319 or 305aa belong to only 1 or 2 sequences.

Check species names of 58 sequences with length 305aa. --> Nothing special observed.
```bash
awk -F"\t" 'NR>1 {if ($3==305) print $0}' fepB.Klebs_pneumo.summary.tsv
```

Extract major sequences with length = 305 or 319aa, then perform clustering.
```bash
awk -v RS=">" -F"[][]" 'NR>1 {
    split($2,sp," ");
    gsub(/\n/,"",$3); 
    if (sp[1]=="Klebsiella" && sp[2]=="pneumoniae" && (length($3)==305 || length($3)==319)) 
    {print ">"$1,$2,"\n"$3}}' fepB.Klebs.sequences.fasta >\
    fepB.Klebs_pneumo.major.sequences.fasta

## Check
fainfo_hc4 fepB.Klebs_pneumo.major.sequences.fasta

## Clustering
conda activate tormes-1.3.0 

cd-hit -i fepB.Klebs_pneumo.major.sequences.fasta \
    -o fepB.Klebs_pneumo.major.cdhit95 -c 0.95 -n 5 -d 0 \
    -M 16000 -T 8

cd-hit -i fepB.Klebs_pneumo.major.sequences.fasta \
    -o fepB.Klebs_pneumo.major.cdhit100 -c 1.00 -n 5 -d 0 \
    -M 16000 -T 8
```

### *Klebsiella pneumoniae* + other Klebs species

Extract sequences with 
- major Kleb pneuomo sequences with length = 305 or 319aa
- \+ other Klebsiella species with clear species name, 

then perform clustering
```bash
defined_sp_seq(){
    action=$1;

    awk -v act=$action -v RS=">" -F"[][]" 'NR>1 {
        split($2,sp," ");
        gsub(/\n/,"",$3);
        check=0;
        if (length(sp)>=2 && sp[1]=="Klebsiella" && sp[2]!="sp."){
            check=1;
            if (sp[2]=="pneumoniae" && length($3)!=305 && length($3)!=319) check=0;
        };
        if (check==1){
            if (act=="describe") print sp[1],sp[2] 
            else print ">"$1,$2,"\n"$3}
        }' \
        fepB.Klebs.sequences.fasta
}
export -f defined_sp_seq

# Species distribution
defined_sp_seq describe | sort | uniq -c

# Extract     
defined_sp_seq extract > fepB.Klebs.defined_sp.sequences.fasta

# Clustering
conda activate tormes-1.3.0 
cd-hit -i fepB.Klebs.defined_sp.sequences.fasta \
    -o fepB.Klebs.defined_sp.cdhit95 -c 0.95 -n 5 -d 0 \
    -M 16000 -T 8

# Summary table of clustering
echo -e cdhit_cluster"\t"accession_number"\t"species"\t"length"\t"identity > fepB.Klebs.defined_sp.cdhit95.tsv
while read -r line; do
    if [[ "$line" =~ \>Cluster* ]]; then
        c=$(echo "$line" | sed 's/>//g');
        # echo "$c";
    else 
        acc=$(echo "$line" | cut -f2 | cut -d " " -f2 | 
                sed 's/>//;s/\.\.\.//');
        len=$(echo "$line" | cut -f2 | cut -d " " -f1 | 
                sed 's/aa,//');
        id=$(echo "$line" | cut -f2 | cut -d " " -f4 |
                sed 's/%//');
        if [ "$id" == "" ]; then id="NA"; fi;
        sp=$(grep -w "$acc" fepB.Klebs.summary.tsv | cut -f2 | cut -d " " -f1,2);
        echo -e $c"\t"$acc"\t"$sp"\t"$len"\t"$id >> \
            fepB.Klebs.defined_sp.cdhit95.tsv;
    fi;
done < fepB.Klebs.defined_sp.cdhit95.clstr
```

## 19 June, 2023: Download sequences for all targeted proteins (including *fepB*)

Search queries on NCBI:
- `((Klebsiella[Organism] AND {gene-name}[Gene Name])) NOT partial[Title]`
- `((Klebsiella[Organism] AND Colicin I receptor[Protein])) NOT partial[Title]`

FASTA sequence files in directory: `fasta_seqs`

## 23/06-05/07, 2023: Examine the sequence databases of all targets

```bash
# import functions
source ./functions.sh
# array of targeted proteins
protein_arr=("fepA" "fepB" "yidR" "colicinI" "fecA" "fimH" "mrkA" "ompA" "ompN" "phoE" "cusC")
```

Check species name for *K. pneumoniae*
```bash
for p in ${protein_arr[@]}; do
    kp_summary fasta_seqs/"$p".Klebs.sequences.fasta | cut -f2 | grep -w "Klebsiella pneumoniae" |\
        sort | uniq -c | sort | less;
done
```

Get top 3 sequence lengths of *K. pneumoniae*
```bash
outfile="summary.top2seqlen.tsv";
echo -e gene"\t"len_1"\t"count_1"\t"percent_1"\t"len_2"\t"count_2"\t"percent_2"\t"sum_percent > $outfile;

for p in ${protein_arr[@]}; do
    # echo "##### "$p" #####";
    # kp_summary fasta_seqs/"$p".Klebs.sequences.fasta | len_dist_from_summary | head -n 4;
    top2=$(kp_summary fasta_seqs/"$p".Klebs.sequences.fasta | len_dist_from_summary | head -n3);
    l1=$(echo $top2 | cut -d " " -f4);
    c1=$(echo $top2 | cut -d " " -f5);
    p1=$(echo $top2 | cut -d " " -f6);
    l2=$(echo $top2 | cut -d " " -f7);
    c2=$(echo $top2 | cut -d " " -f8);
    p2=$(echo $top2 | cut -d " " -f9);
    t=$(bc -l <<< ${p1}+${p2});
    echo -e $p"\t"$l1"\t"$c1"\t"$p1"\t"$l2"\t"$c2"\t"$p2"\t"$t >> $outfile;
done
```

2 proteins that have too little number of sequences --> excluded in analyses: *yidR* (n=22) and *ompN* (n=176)
2 proteins that have 2 top sequence lengths with the 2nd has relatively high proportions:
- *fecA* (701 aa: n=1303, 57.25%; 774 aa: n=835, 36.69%)
- *fimH* (302 aa: n=1725, 82.22%; 301 aa: n=337, 16.06%)

## Analyse proteins with high-quality sequence databases (n=7, the "Seven" ones)

### 1. Clustering Kp-only sequences (23/06-05/07, 2023)
Objectives:
- Are the protein sequences conserved in *K. pneumoniae* (Kp)?
- If yes for above, are the conserved Kp sequences divergent from *E. coli* protein sequences?

```bash
# import functions
source ./functions.sh
# array of targeted proteins
protein_arr=("fepA" "fepB" "colicinI" "mrkA" "ompA" "phoE" "cusC")

# iterate throught the list of proteins
conda activate tormes-1.3.0 
for p in ${protein_arr[@]}; do
    mkdir -p $p;
    fasta_file="../fasta_seqs/${p}.Klebs.sequences.fasta";

    (
        cd $p;
        # Summary file for all sequences
        summary_file="${p}.Klebs.summary.tsv";
        summary_from_fasta $fasta_file > $summary_file;
        
        # Get top 2 seq length of K. pneumoniae
        IFS=";" read -r -a toptwo <<< $(kp_summary $fasta_file | len_dist_from_summary | \
                head -n 3 | tail -n +2 | cut -f1 | tr "\n" ";");
        
        # Extract major K. pneumoniae sequences (top 2 seq length)
        extract_majorKp_seq $fasta_file "${toptwo[@]}" >\
            "$p".Kp.major.sequences.fasta;

        # Clustering
        cd-hit -i "$p".Kp.major.sequences.fasta \
            -o "$p".Kp.major.cdhit95 -c 0.95 -n 5 -d 0 \
            -M 16000 -T 8;
        summary_clustering "$p".Kp.major.cdhit95.clstr $summary_file "$p".Kp.major.cdhit95.tsv;
    );
done
```

Check the number of clusters
```bash
protein_arr=("fepA" "fepB" "colicinI" "mrkA" "ompA" "phoE" "cusC")
for p in ${protein_arr[@]}; do
    file="${p}/${p}.Kp.major.cdhit95.tsv";
    echo "----- "$p" -----"
    tail -n +2 $file | cut -f1,4 | sort | uniq -c;
done
```

- 1 cluster: 
    - *mrkA*: 202aa (n=1791), 193aa (n=6) 
    - *phoE*: 350aa (n=2861), 352aa (n=2)
- 2 clusters: 
    - *fepA*: (n=12, 752aa); (m=1689, 742aa)
    - *colicin I* (n=219, 687aa); (m=4016, 657aa)
    - *ompA* (n=5138, 356aa); (m=169, 160aa)  
- 3 clusters: 
    - *fepB* (n=5223, m=1, k=1)
    - *cusC* (n=2771, m=4, k=57)

3 clusters of *cusC*:
- Cluster 0 (n=2771): 497aa (n=793) and 461aa (n=1978)
- Cluster 1 (m=4): 497aa
- Cluster 2 (k=57): 461aa
- Representative seq of Cluster 2 is very different from those of the other clusters.
- Representative seq of Cluster 0 & 1 (497aa) share 94.97% identity (100% coverage, BLASTP)

See a summary of the results from this section in `table.seven.Kp_clusters.md`

### 2. Cluster sequences of largest Kp clusters with non-Kp sequences (05/07-07/07, 2023)

Arrays of the Seven proteins, and their largest Kp clusters to include with the non-Kp sequences.

```bash
protein_arr=("mrkA" "phoE" "fepA" "colicinI" "ompA" "fepB" "cusC")
clstr_arr=("0" "0" "1" "1" "0" "0" "0")
```

Extract the sequences, then cluster them

```bash
source ./functions.sh
conda activate tormes-1.3.0 

for i in ${!protein_arr[@]}; do
    p=${protein_arr[$i]};
    c=${clstr_arr[$i]};
    
    (
        cd $p/;
        clstr_file="${p}.Kp.major.cdhit95.tsv";
        Kp_fasta="${p}.Kp.major.sequences.fasta";
        idlist_file="${p}.Kp_cluster${c}.list";
        ori_fasta="../fasta_seqs/${p}.Klebs.sequences.fasta";
        outfile="${p}.Kp_cluster${c}.nonKp.fasta";
        summary_file="${p}.Klebs.summary.tsv";

        # Extract seq from largest Kp cluster
        grep -w "Cluster ${c}" $clstr_file | cut -f2 > $idlist_file;

        seq_from_IDlist $Kp_fasta $idlist_file > $outfile; 
        extract_defined_nonKp_seq $ori_fasta >> $outfile;

        # Clustering
        cd-hit -i $outfile \
            -o ${p}.Kp_cluster${c}.nonKp.cdhit95 -c 0.95 -n 5 -d 0 \
            -M 16000 -T 8;
        summary_clustering ${p}.Kp_cluster${c}.nonKp.cdhit95.clstr $summary_file \
            ${p}.Kp_cluster${c}.nonKp.cdhit95.tsv;
    );    
done
```

Check which cluster(s) the Kp sequences are in and the Kp sequence numbers in the clusters.  
Expected to see that all Kp seqs are in only 1 cluster. Otherwise, it would suggest that the Kp sequences are not distinctive from other non-Kp seqs, especially when the number of Kp seqs in the "unexpected" clusters are high.
```bash
for p in ${protein_arr[@]}; do
    echo "----- ${p} -----";
    grep -w "Klebsiella pneumoniae" ${p}/*.nonKp.cdhit95.tsv | cut -f1 | sort | uniq -c;
done
```

## Analyze *fecA* and *fimH* (10/07/2023)

2 proteins that have 2 top sequence lengths with the 2nd has relatively high proportions:
- *fecA* (701 aa: n=1303, 57.25%; 774 aa: n=835, 36.69%)
- *fimH* (302 aa: n=1725, 82.22%; 301 aa: n=337, 16.06%)

```bash
# import functions
source ./functions.sh
# array of targeted proteins
protein_arr=("fecA" "fimH")

# iterate throught the list of proteins
conda activate tormes-1.3.0 
for p in ${protein_arr[@]}; do
    mkdir -p $p;
    fasta_file="../fasta_seqs/${p}.Klebs.sequences.fasta";

    (
        cd $p;
        # Summary file for all sequences
        summary_file="${p}.Klebs.summary.tsv";
        summary_from_fasta $fasta_file > $summary_file;
        
        # Get top 2 seq length of K. pneumoniae
        IFS=";" read -r -a toptwo <<< $(kp_summary $fasta_file | len_dist_from_summary | \
                head -n 3 | tail -n +2 | cut -f1 | tr "\n" ";");
        
        # Extract major K. pneumoniae sequences (top 2 seq length)
        extract_majorKp_seq $fasta_file "${toptwo[@]}" >\
            "$p".Kp.major.sequences.fasta;

        # Clustering
        cd-hit -i "$p".Kp.major.sequences.fasta \
            -o "$p".Kp.major.cdhit95 -c 0.95 -n 5 -d 0 \
            -M 16000 -T 8;
        summary_clustering "$p".Kp.major.cdhit95.clstr $summary_file "$p".Kp.major.cdhit95.tsv;
    );
done
```

Check the number of clusters
```bash
protein_arr=("fecA" "fimH")
for p in ${protein_arr[@]}; do
    file="${p}/${p}.Kp.major.cdhit95.tsv";
    echo "----- "$p" -----"
    tail -n +2 $file | cut -f1,4 | sort | uniq -c;
done
```

Extract the sequences from largest Kp clusters and non-Kp sequences, then cluster them

```bash
source ./functions.sh
conda activate tormes-1.3.0 

protein_arr=("fimH" "fecA")
clstr_arr=("0" "1")

for i in ${!protein_arr[@]}; do
    p=${protein_arr[$i]};
    c=${clstr_arr[$i]};
    
    (
        cd $p/;
        clstr_file="${p}.Kp.major.cdhit95.tsv";
        Kp_fasta="${p}.Kp.major.sequences.fasta";
        idlist_file="${p}.Kp_cluster${c}.list";
        ori_fasta="../fasta_seqs/${p}.Klebs.sequences.fasta";
        outfile="${p}.Kp_cluster${c}.nonKp.fasta";
        summary_file="${p}.Klebs.summary.tsv";

        # Extract seq from largest Kp cluster
        grep -w "Cluster ${c}" $clstr_file | cut -f2 > $idlist_file;

        seq_from_IDlist $Kp_fasta $idlist_file > $outfile; 
        extract_defined_nonKp_seq $ori_fasta >> $outfile;

        # Clustering
        cd-hit -i $outfile \
            -o ${p}.Kp_cluster${c}.nonKp.cdhit95 -c 0.95 -n 5 -d 0 \
            -M 16000 -T 8;
        summary_clustering ${p}.Kp_cluster${c}.nonKp.cdhit95.clstr $summary_file \
            ${p}.Kp_cluster${c}.nonKp.cdhit95.tsv;
    );    
done

for p in ${protein_arr[@]}; do
    echo "----- ${p} -----";
    grep -w "Klebsiella pneumoniae" ${p}/*.nonKp.cdhit95.tsv | cut -f1 | sort | uniq -c;
done
```

## Analyze *yidR* and *ompN* (10/07/23)

*yidR* (n=22) and *ompN* (n=176)

```bash
# import functions
source ./functions.sh
# array of targeted proteins
protein_arr=("yidR" "ompN")

# iterate throught the list of proteins
conda activate tormes-1.3.0 
for p in ${protein_arr[@]}; do
    mkdir -p $p;
    fasta_file="../fasta_seqs/${p}.Klebs.sequences.fasta";

    (
        cd $p;
        # Summary file for all sequences
        summary_file="${p}.Klebs.summary.tsv";
        summary_from_fasta $fasta_file > $summary_file;

        # Clustering
        cd-hit -i $fasta_file \
            -o "$p".Klebs.cdhit95 -c 0.95 -n 5 -d 0 \
            -M 16000 -T 8;
        summary_clustering "$p".Klebs.cdhit95.clstr $summary_file "$p".Klebs.cdhit95.tsv;
    );
done
```

## Clustering Kp+non-Kp at 98% identity threshold (19/07/23)

```bash
protein_arr=("ompA" "fecA")
clstr_arr=("0" "1")

source ./functions.sh
conda activate tormes-1.3.0 

for i in ${!protein_arr[@]}; do
    p=${protein_arr[$i]};
    c=${clstr_arr[$i]};
    
    (
        cd $p/;
        clstr_file="${p}.Kp.major.cdhit95.tsv";
        Kp_fasta="${p}.Kp.major.sequences.fasta";
        idlist_file="${p}.Kp_cluster${c}.list";
        ori_fasta="../fasta_seqs/${p}.Klebs.sequences.fasta";
        outfile="${p}.Kp_cluster${c}.nonKp.fasta";
        summary_file="${p}.Klebs.summary.tsv";

        # Extract seq from largest Kp cluster
        grep -w "Cluster ${c}" $clstr_file | cut -f2 > $idlist_file;

        seq_from_IDlist $Kp_fasta $idlist_file > $outfile; 
        extract_defined_nonKp_seq $ori_fasta >> $outfile;

        # Clustering
        cd-hit -i $outfile \
            -o ${p}.Kp_cluster${c}.nonKp.cdhit98 -c 0.98 -n 5 -d 0 \
            -M 16000 -T 8;
        summary_clustering ${p}.Kp_cluster${c}.nonKp.cdhit98.clstr $summary_file \
            ${p}.Kp_cluster${c}.nonKp.cdhit98.tsv;
    );    
done
```
